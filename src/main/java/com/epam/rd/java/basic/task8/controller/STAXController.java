package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Flower;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

    private String xmlFileName;
    List<Flower> flowerList = new ArrayList<>();
    Flower currentFlower;

    public List<Flower> getFlowerList() {
        return flowerList;
    }

    public STAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
		try {
			readFromXMLToList(new File(xmlFileName));
		} catch (FileNotFoundException | XMLStreamException e) {
			e.printStackTrace();
		}
	}

	private void readFromXMLToList(File file)
			throws FileNotFoundException, XMLStreamException {

		XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
		XMLStreamReader reader = xmlInputFactory.createXMLStreamReader(
				new FileInputStream(file));

		int eventType;
		while (reader.hasNext()) {
			eventType = reader.next();
			if (eventType == XMLEvent.START_ELEMENT) {

				switch (reader.getName().getLocalPart()) {

					case "flower":
						currentFlower = new Flower();
						break;

					case "name":
						eventType = reader.next();
						if (eventType == XMLEvent.CHARACTERS) {
							currentFlower.setName(reader.getText());
						}
						break;

					case "soil":
						eventType = reader.next();
						if (eventType == XMLEvent.CHARACTERS) {
							currentFlower.setSoil(reader.getText());
						}
						break;

					case "origin":
						eventType = reader.next();
						if (eventType == XMLEvent.CHARACTERS) {
							currentFlower.setOrigin(reader.getText());
						}
						break;

					case "stemColour":
						eventType = reader.next();
						if (eventType == XMLEvent.CHARACTERS) {
							currentFlower.setStemColour(reader.getText());
						}
						break;

					case "leafColour":
						eventType = reader.next();
						if (eventType == XMLEvent.CHARACTERS) {
							currentFlower.setLeafColour(reader.getText());
						}
						break;

					case "aveLenFlower":
						eventType = reader.next();
						if (eventType == XMLEvent.CHARACTERS) {
							currentFlower.setAveLenFlower(reader.getText());
						}
						break;

					case "tempreture":
						eventType = reader.next();
						if (eventType == XMLEvent.CHARACTERS) {
							currentFlower.setTempreture(reader.getText());
						}
						break;

					case "lighting":
						currentFlower.setLighting(reader.getAttributeValue(null, "lightRequiring"));
						break;

					case "watering":
						eventType = reader.next();
						if (eventType == XMLEvent.CHARACTERS) {
							currentFlower.setWatering(reader.getText());
						}
						break;

					case "multiplying":
						eventType = reader.next();
						if (eventType == XMLEvent.CHARACTERS) {
							currentFlower.setMultiplying(reader.getText());
						}
						break;
				}

			}

			if (eventType == XMLEvent.END_ELEMENT) {
				if (reader.getName().getLocalPart().equals("flower")) {
					flowerList.add(currentFlower);
				}
			}

		}
	}

}
