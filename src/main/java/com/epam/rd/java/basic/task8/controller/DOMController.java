package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.entity.Flower;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * Controller for DOM parser.
 */
public class DOMController {

    private String xmlFileName;

    final List<Flower> flowers = new ArrayList<>();

    public List<Flower> getFlowers() {
        return flowers;
    }

    public DOMController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
        readFromXML();
    }

    private void readFromXML() {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setNamespaceAware(true);

        DocumentBuilder db = null;
        try {
            db = dbf.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }

        Document doc = null;
        try {
            doc = db.parse(new File(xmlFileName));
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        NodeList list = doc.getElementsByTagName("flower");

        for (int j = 0; j < list.getLength(); j++) {
            Node node = list.item(j);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element el = (Element) node;
                Flower f = new Flower();
                f.setName(el.getElementsByTagName("name").item(0).getTextContent());
                f.setSoil(el.getElementsByTagName("soil").item(0).getTextContent());
                f.setOrigin(el.getElementsByTagName("origin").item(0).getTextContent());
                f.setStemColour(el.getElementsByTagName("stemColour").item(0).getTextContent());
                f.setLeafColour(el.getElementsByTagName("leafColour").item(0).getTextContent());
                f.setAveLenFlower(el.getElementsByTagName("aveLenFlower").item(0).getTextContent());
                f.setTempreture(el.getElementsByTagName("tempreture").item(0).getTextContent());
                f.setLighting(el.getElementsByTagName("lighting").item(0).getAttributes().getNamedItem("lightRequiring").getTextContent());
                f.setWatering(el.getElementsByTagName("watering").item(0).getTextContent());
                f.setMultiplying(el.getElementsByTagName("multiplying").item(0).getTextContent());

                flowers.add(f);
            }
        }

    }

}
