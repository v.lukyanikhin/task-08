package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Flower;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {

    private String xmlFileName;

    private StringBuilder currentValue = new StringBuilder();
    List<Flower> flowerList;
    Flower currentFlower;


    public SAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public List<Flower> getFlowerList() {
        return flowerList;
    }

    @Override
    public void startDocument() {
        flowerList = new ArrayList<>();
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {

        currentValue.setLength(0);

        if (qName.equalsIgnoreCase("flower")) {
            currentFlower = new Flower();
        }

        if (qName.equalsIgnoreCase("lighting")) {
            String value = attributes.getValue("lightRequiring");
            currentFlower.setLighting(value);
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        if (qName.equalsIgnoreCase("name")) {
            currentFlower.setName(currentValue.toString());
        }
        if (qName.equalsIgnoreCase("soil")) {
            currentFlower.setSoil(currentValue.toString());
        }
        if (qName.equalsIgnoreCase("origin")) {
            currentFlower.setOrigin(currentValue.toString());
        }
        if (qName.equalsIgnoreCase("stemColour")) {
            currentFlower.setStemColour(currentValue.toString());
        }
        if (qName.equalsIgnoreCase("leafColour")) {
            currentFlower.setLeafColour(currentValue.toString());
        }
        if (qName.equalsIgnoreCase("multiplying")) {
            currentFlower.setMultiplying(currentValue.toString());
        }
        if (qName.equalsIgnoreCase("flower")) {
            flowerList.add(currentFlower);
        }
        if (qName.equalsIgnoreCase("aveLenFlower")) {
            currentFlower.setAveLenFlower(currentValue.toString());
        }
        if (qName.equalsIgnoreCase("tempreture")) {
            currentFlower.setTempreture(currentValue.toString());
        }
        if (qName.equalsIgnoreCase("watering")) {
            currentFlower.setWatering(currentValue.toString());
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        currentValue.append(ch, start, length);
    }
}