package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}

		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		//DOM
		DOMController domController = new DOMController(xmlFileName);
		String outputXmlFile = "output.dom.xml";
		XMLWriter.writeXmlToFile(XMLFileCreator.createXMLDoc(domController.getFlowers()), new File(outputXmlFile));
		//SAX
		SAXController saxController = new SAXController(xmlFileName);
		SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
		SAXParser saxParser = saxParserFactory.newSAXParser();
		saxParser.parse(new File(xmlFileName),saxController);
		outputXmlFile = "output.sax.xml";
		XMLWriter.writeXmlToFile(XMLFileCreator.createXMLDoc(saxController.getFlowerList()),new File(outputXmlFile));
		//STAX
		STAXController staxController = new STAXController(xmlFileName);
		outputXmlFile = "output.stax.xml";
		XMLWriter.writeXmlToFile(XMLFileCreator.createXMLDoc(staxController.getFlowerList()),new File(outputXmlFile));
	}
}
