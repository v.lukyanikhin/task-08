package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.Flower;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.util.List;

public class XMLFileCreator {
    public static Document createXMLDoc(List<Flower> flowers) throws ParserConfigurationException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.newDocument();

        //addingRootElement
        Element root = doc.createElement("flowers");
        root.setAttribute("xmlns", "http://www.nure.ua");
        root.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        root.setAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd");
        doc.appendChild(root);

        Element element;
        Element element2;
        for (Flower f : flowers) {
            //Adding mainInfo
            Element flower = doc.createElement("flower");
            element = doc.createElement("name");
            element.setTextContent(f.getName());
            flower.appendChild(element);

            element = doc.createElement("soil");
            element.setTextContent(f.getSoil());
            flower.appendChild(element);

            element = doc.createElement("origin");
            element.setTextContent(f.getOrigin());
            flower.appendChild(element);
            //addingVisualParameters
            element = doc.createElement("visualParameters");

            element2 = doc.createElement("stemColour");
            element2.setTextContent(f.getStemColour());
            element.appendChild(element2);

            element2 = doc.createElement("leafColour");
            element2.setTextContent(f.getLeafColour());
            element.appendChild(element2);

            element2 = doc.createElement("aveLenFlower");
            element2.setAttribute("measure", "cm");
            element2.setTextContent(f.getAveLenFlower());
            element.appendChild(element2);
            flower.appendChild(element);
            //addingGrowingTips
            element = doc.createElement("growingTips");

            element2 = doc.createElement("tempreture");
            element2.setAttribute("measure", "celcius");
            element2.setTextContent(f.getTempreture());
            element.appendChild(element2);

            element2 = doc.createElement("lighting");
            element2.setAttribute("lightRequiring", f.getLighting());
            element.appendChild(element2);

            element2 = doc.createElement("watering");
            element2.setAttribute("measure", "mlPerWeek");
            element2.setTextContent(f.getWatering());
            element.appendChild(element2);

            flower.appendChild(element);
            //addingRestElements
            element = doc.createElement("multiplying");
            element.setTextContent(f.getMultiplying());
            flower.appendChild(element);
            //finishFlowerElement
            root.appendChild(flower);
        }
        return doc;
    }
}
